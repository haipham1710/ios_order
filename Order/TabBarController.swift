//
//  TabBarController.swift
//  Order
//
//  Created by admin on 11/2/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.global(qos: .background).async {
            self.SyncCustomer()
            
        }
                // Do any additional setup after loading the view.
    }
    //download new customer on server and upload new customer on local
    func SyncCustomer()
    {
        if(Utils.isConnectedToNetwork())
        {
            Fetcher.customerByLastUpdate(date: SQLiteHelper.getLastSync()) { (customerList: [CustomerModel]?, error: NSError?) in
                if(customerList != nil)
                {
                    // insert data
                    SQLiteHelper.syncCustomers(customers: customerList!)
                    // end insert data
                }
            }
            //if has new customer in local, sync to server and delete in local
            let customerList : Array<CustomerModel> = SQLiteHelper.getCustomersOffline()
            if(customerList.count != 0)
            {
                for customer in customerList {
                    Fetcher.insertCustomer(customer: customer, results: { (result, error) in
                        if(result != nil)
                        {
                            SQLiteHelper.deleteCustomerOffline(phone: customer.numberPhone!)
                        }
                        
                    })
                }
            }
        }
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
