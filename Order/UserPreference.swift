//
//  UserPreference.swift
//  Order
//
//  Created by Super on 10/21/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation

class UserPreference: NSObject {
    
    class func getPreference(_ key : String) -> AnyObject? {
        let prefs = UserDefaults.standard;
        return prefs.object(forKey: key) as AnyObject?;
    }
    
    class func setPreference(_ key: String,_ value: AnyObject?) -> Void{
        let prefs = UserDefaults.standard;
        prefs.set(value, forKey: key);
        prefs.synchronize();
    }
    
    class var userName : String? {
        set {
            setPreference("username", newValue as AnyObject?);
        }
        get {
            return getPreference("username") as! String?;
        }
    }
    
    class var token : String? {
        set {
            setPreference("token", newValue as AnyObject?);
        }
        get {
            return getPreference("token") as! String?;
        }
    }
    
    
    class var password : String? {
        set {
            setPreference("password", newValue as AnyObject?);
        }
        get {
            return getPreference("password") as! String?;
        }
    }
    
}
