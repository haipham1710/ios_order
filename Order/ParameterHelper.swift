//
//  ParameterHelper.swift
//  GoVap
//
//  Created by ADMIN on 1/21/15.
//  Copyright (c) 2015 TMT Solution. All rights reserved.
//

import UIKit

class ParameterHelper {
    var error: NSError?;
    var json : NSMutableDictionary = NSMutableDictionary();
    
    func add(_ name:String,_ string:String?) -> ParameterHelper {
        if (string != nil && !string!.isEmpty) {
            json.setValue(string, forKey: name);
        }
        return self;
    }
    
    func add(_ name:String,_ value:Int) -> ParameterHelper {
        json.setValue(NSNumber(value: value as Int), forKey: name);
        return self;
    }
    
    func add(_ name:String,_ value:Bool) -> ParameterHelper {
        json.setValue(value ? "true" : "false", forKey: name);
        return self;
    }
    
    func add(_ name:String,_ value:Bool?) -> ParameterHelper {
        if (value != nil) {
            json.setValue(value! ? "true" : "false", forKey: name);
        }
        return self;
    }
    
    func add(_ name:String,_ value:Double) -> ParameterHelper {
        json.setValue(NSNumber(value: value as Double), forKey: name);
        return self;
    }
    
    func add(_ name:String,_ value:Int?) -> ParameterHelper {
        if (value != nil) {
            json.setValue(NSNumber(value: value! as Int), forKey: name);
        }
        return self;
    }
    
    func add(_ name:String,_ value:Date) -> ParameterHelper {
        json.setValue(JsonHelper.iso8601Formatter.string(from: value) , forKey: name);
        return self;
    }
    
    func add(_ name:String,_ value:Date?) -> ParameterHelper {
        if (value != nil) {
            add(name, value!);
        }
        return self;
    }
    
    func add(_ name:String,_ value:Double?) -> ParameterHelper {
        if (value != nil) {
            json.setValue(NSNumber(value: value! as Double), forKey: name);
        }
        return self;
    }
    
    var count : Int {
        get {
            return json.count;
        }
    }
    
    var rawData : Data? {
        get {
            do
            {
                return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted);
            }
            catch let error as NSError
            {
                print(error.localizedDescription)
                return nil
            }
        
        }
    }
    
    var description : String {
        get {
            return json.description;
        }
    }
}

