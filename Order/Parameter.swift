//
//  Parameter.swift
//  FirstSwift
//
//  Created by ADMIN on 2/3/15.
//  Copyright (c) 2015 TMT Solution. All rights reserved.
//

import UIKit

protocol Parameter {
    func toParameter(_ helper:ParameterHelper);
}
