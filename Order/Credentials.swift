//
//  Credentials.swift
//  Order
//
//  Created by Super on 10/20/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
class Credentials {
    fileprivate var header: String
    fileprivate var value: String
    init(header: String,value: String)
    {
        self.header = header
        self.value = value
    }
    func addCredentials(_ request: NSMutableURLRequest)
    {
        request.addValue(value, forHTTPHeaderField: header)
    }
    func hasObject()->Bool
    {
        return header != ""
    }
}
