//
//  LoginController.swift
//  Order
//
//  Created by Lam Xuan on 10/19/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit

class LoginController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var tf_password: UITextField!
    @IBOutlet weak var tf_username: UITextField!
    @IBOutlet weak var bt_login: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        bt_login.addTarget(self, action: #selector(actionLogin), for: .touchUpInside)        // Do any additional setup after loading the view.
        tf_password.text = UserPreference.password
        tf_username.text = UserPreference.userName
        tf_password.delegate = self
        tf_username.delegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if Utils.isConnectedToNetwork() {
            print("Internet connected")
            
        }
        else
        {
            MessageBox.show("Offline Mode", "")
            print("Internet connection FAILED")
            self.performSegue(withIdentifier: "loginSuccess", sender: self)
            
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func actionLogin(_ action:UIButton)
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let username: String = tf_username.text!
        let password: String = tf_password.text!
        var error: Int = 0
        if(username.isEmpty)
        {
            error = 1
        }
        else if(password.isEmpty)
        {
            error = 2
        }
        switch error {
        case 1:
            tf_password.layer.borderWidth = 0
            tf_username.layer.borderWidth = 1
            tf_username.layer.borderColor = UIColor.red.cgColor
            return
        case 2:
            tf_username.layer.borderWidth = 0
            tf_password.layer.borderWidth = 1
            tf_password.layer.borderColor = UIColor.red.cgColor
            return
        default:
            break
        }

        let params : ParameterHelper = ParameterHelper()
        params.add("username", username)
        params.add("password", password)
        params.add("grant_type", "password")
        Fetcher.token(params: params, tokenModel: {(tokenModel: TokenModel?,error: NSError?) -> Void in
            //print(tokenModel.access_token)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if(tokenModel != nil)
            {
                if(!tokenModel!.access_token.isEmpty)
                {
                    UserPreference.password = password
                    UserPreference.userName = username
                    UserPreference.token = tokenModel!.access_token
                    self.performSegue(withIdentifier: "loginSuccess", sender: self)
                }
                else{
                    MessageBox.show(tokenModel!.error_description, "Error")
                }
            }
            else
            {
                MessageBox.show("Vui lòng kiểm tra tài khoản, mật khẩu hoặc server", "Lỗi")
            }

        })

    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == tf_username)
        {
            tf_username.layer.borderWidth=0
        } else if( textField == tf_password)
        {
            tf_password.layer.borderWidth=0
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == tf_username) {
            tf_password.becomeFirstResponder()
        } else {
            //remove keyboard
            tf_password.resignFirstResponder()
        }
        return false
    }


}
