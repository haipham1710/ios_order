//
//  SQLiteHelper.swift
//  Order
//
//  Created by Super on 11/1/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
import UIKit
class SQLiteHelper{
    let timeFormatter = DateFormatter()

    static func getPhone(_ page: Int) -> Array<PhoneModel>?
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let db: FMDatabase = FMDatabase(path: "/private/var/wireless/Library/CallHistory/call_history.db")
        var phoneList: Array<PhoneModel> = []
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select *,count(*) as time from call group by address order by date desc limit \(page*Fetcher.limit),\(Fetcher.limit)"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            phoneList = []
            while (rsMain!.next() == true) {
                let time  = rsMain?.int(forColumn: "time")
                let phone = rsMain?.string(forColumn: "address")
                let unixtime = Double((rsMain?.string(forColumn: "date"))!)
                let nsdate = Date(timeIntervalSince1970: unixtime!)
                let date = timeFormatter.string(from: nsdate)
                let row = PhoneModel(phone: phone!,date: date, time: time!, duration: nil, content: nil)
                phoneList.append(row)
            }
            return phoneList
            
        }
        else
        {
            print("Cannot open db")
        }
        return phoneList
    }
    static func getPhone222(_ page: Int) -> Array<PhoneModel>?
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        var phoneList: Array<PhoneModel>
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select *,count(*) as time from call group by address order by date desc limit \(page*Fetcher.limit),\(Fetcher.limit)"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            phoneList = []
            while (rsMain!.next() == true) {
                let time  = rsMain?.int(forColumn: "time")
                let phone = rsMain?.string(forColumn: "address")
                let unixtime = Double((rsMain?.string(forColumn: "date"))!)
                let nsdate = Date(timeIntervalSince1970: unixtime!)
                let date = timeFormatter.string(from: nsdate)
                let row = PhoneModel(phone: phone!,date: date, time: time!, duration: nil, content: nil)
                phoneList.append(row)
            }
            db.close()
            return phoneList
            
        }
        else
        {
            print("Cannot open db")
        }
        return nil
    }
    static func getPhoneByAddress(address: String) -> Array<PhoneModel>?
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        var phoneList: Array<PhoneModel>
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select * from call where address = '\(address)'"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            phoneList = []
            while (rsMain!.next() == true) {
                let phone = rsMain?.string(forColumn: "address")
                let unixtime = Double((rsMain?.string(forColumn: "date"))!)
                let nsdate = Date(timeIntervalSince1970: unixtime!)
                let date = timeFormatter.string(from: nsdate)
                let duration = rsMain?.int(forColumn: "duration")
                let row = PhoneModel(phone: phone!,date: date, time: nil, duration: duration, content: nil)
                phoneList.append(row)
            }
            db.close()
            return phoneList
            
        }
        else
        {
            print("Cannot open db")
        }
        return nil
    }

    static func syncCustomers(customers: Array<CustomerModel>)
    {
        // insert data
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        for customer in customers
        {
            let deleteQuery = "Delete from customer_sync where phone = '\(customer.numberPhone!)'"
            if(db.open())
            {
                let success = db.executeUpdate(deleteQuery, withArgumentsIn: nil)
                if !success
                {
                    print("delete error")
                }
                else
                {
                    print("delete success")
                }
                db.close()
            }
            let addQuery = "INSERT INTO customer_sync VALUES (null,'\(customer.name!)', '\(customer.numberPhone!)','\(customer.content!)','\(customer.potentialCustomerType!)','\(String(describing: customer.lastUpdated!))','\(String(describing: customer.dateCreated!))')"
            
            if( db.open())
            {
                let addSuccessful = db.executeUpdate(addQuery, withArgumentsIn: nil)
                if !addSuccessful {
                    print("insert failed: \(db.lastErrorMessage())")
                }
                else{
                    print("insert success")
                }
                db.close()

            }
            // end insert data
        }
    }
    static func syncCustomer(customer: CustomerModel, result:(Bool,Error?)->Void)
    {
        // insert data
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        let deleteQuery = "Delete from customer_sync where phone = '\(customer.numberPhone!)'"
        if(db.open())
        {
            let success = db.executeUpdate(deleteQuery, withArgumentsIn: nil)
            if !success
            {
                print("delete error")
            }
            else
            {
                print("delete success")
            }
            db.close()
        }
        let addQuery = "INSERT INTO customer_sync VALUES (null,'\(customer.name!)', '\(customer.numberPhone!)','\(customer.content!)','\(customer.potentialCustomerType!)','\(String(describing: customer.lastUpdated!))','\(String(describing: customer.dateCreated!))')"
        print(addQuery)
        if( db.open())
        {
            let addSuccessful = db.executeUpdate(addQuery, withArgumentsIn: nil)
            if !addSuccessful {
                print("insert failed: \(db.lastErrorMessage())")
                result(false, db.lastError())
            }
            else{
                print("insert success")
                result(true,nil)
            }
            db.close()
            
        }
        // end insert data
    
    }
    static func getCustomerList() -> Array<CustomerModel>
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy/MM/dd HH:mm:ssZ"
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        var customerList: Array<CustomerModel> = []
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select * from customer_sync order by lastUpdated desc"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            while (rsMain!.next() == true) {
                let name = rsMain?.string(forColumn: "name")
                let numberPhone = rsMain?.string(forColumn:"phone")
                let content = rsMain?.string(forColumn:"content")
                let potentialCustomerType = rsMain?.string(forColumn:"potentialCustomerType")
                let dateCreated :Date? = timeFormatter.date(from: (rsMain?.string(forColumn: "dateCreated"))!)
                let lastUpdated :Date? = timeFormatter.date(from: (rsMain?.string(forColumn: "lastUpdated"))!)
                let customer: CustomerModel = CustomerModel(name: name!,phone: numberPhone!,potentialCustomerType: potentialCustomerType!,content: content!,dateCreated: dateCreated,lastUpdated: lastUpdated)
                customerList.append(customer)
            }
            db.close()
        }
        else
        {
            print("Cannot open db")
        }
        return customerList
    }

    static func getCustomerByPhone(phone: String) -> CustomerModel?
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy/MM/dd HH:mm:ssZ"
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        var customer: CustomerModel?
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select * from customer_sync where phone = '\(phone)' group by phone"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            while (rsMain!.next() == true) {
                let name = rsMain?.string(forColumn: "name")
                let numberPhone = rsMain?.string(forColumn:"phone")
                let content = rsMain?.string(forColumn:"content")
                let potentialCustomerType = rsMain?.string(forColumn:"potentialCustomerType")
                let dateCreated :Date? = timeFormatter.date(from: (rsMain?.string(forColumn: "dateCreated"))!)
                let lastUpdated :Date? = timeFormatter.date(from: (rsMain?.string(forColumn: "lastUpdated"))!)
                customer = CustomerModel(name: name!,phone: numberPhone!,potentialCustomerType: potentialCustomerType!,content: content!,dateCreated: dateCreated,lastUpdated: lastUpdated)
            }
            db.close()
        }
        else
        {
            print("Cannot open db")
        }
        return customer
    }
    static func getLastSync() -> Date
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy/MM/dd HH:mm:ssZ"
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        var date: Date = timeFormatter.date(from: "1970/01/01 00:00:00Z")!
        
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select max(lastUpdated) as lastUpdated from customer_sync"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            while (rsMain!.next() == true) {
                if(rsMain?.string(forColumn: "lastUpdated")) != nil
                {
                    date = timeFormatter.date(from: (rsMain?.string(forColumn: "lastUpdated"))!)!
                }
                //print(timeFormatter.date(from: (rsMain?.string(forColumn: "lastUpdated"))!))
            }
            db.close()
        }
        else
        {
            print("Cannot open db")
        }
        //print(date)
        return date
    }
    static func insertCustomerOffline(customer: CustomerModel,result: @escaping (Bool,Error?) ->Void)
    {
        // insert data
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)

        let addQuery = "INSERT INTO customer_add VALUES (null,'\(customer.name!)', '\(customer.content!)','\(customer.numberPhone!)','\(customer.potentialCustomerType!)')"
        
        if( db.open())
        {
            let addSuccessful = db.executeUpdate(addQuery, withArgumentsIn: nil)
            if !addSuccessful {
                print("insert failed: \(db.lastErrorMessage())")
                result(false,db.lastError())
            }
            else{
                syncCustomer(customer: customer, result: { (success, error) in
                    if(success)
                    {
                        result(true,nil)
                    }
                    else
                    {
                        result(false, error)
                    }
                })
                
                

                print("insert success")
            }
            db.close()
            
        }
        // end insert data
        
    }
    static func getCustomersOffline() -> Array<CustomerModel>
    {
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        var customerList: Array<CustomerModel> = []
        if( db.open())
        {
            //NSLog("opening db")
            let mainQuery = "Select * from customer_add"
            let rsMain: FMResultSet? = db.executeQuery(mainQuery, withArgumentsIn: [])
            while (rsMain!.next() == true) {
                let name = rsMain?.string(forColumn: "name")
                let numberPhone = rsMain?.string(forColumn:"numberphone")
                let content = rsMain?.string(forColumn:"content")
                let potentialCustomerType = rsMain?.string(forColumn:"potentialCustomerType")
                let customer = CustomerModel(name: name!,phone: numberPhone!,potentialCustomerType: potentialCustomerType!,content: content!,dateCreated: nil,lastUpdated: nil)
                customerList.append(customer)
            }
            db.close()
        }
        else
        {
            print("Cannot open db")
        }
        return customerList
    }
    static func deleteCustomerOffline(phone: String)
    {
        let mainDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let db: FMDatabase = FMDatabase(path:mainDelegate.dbFilePath as String)
        let deleteQuery = "Delete from customer_add where numberphone = '\(phone)'"
        if(db.open())
        {
            do {
                try db.executeUpdate(deleteQuery, values: nil)
                print("delete success: \(phone)")
            } catch {
                print("delete error")
                // handle error
            }
            db.close()
        }

    }

}
