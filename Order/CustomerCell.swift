//
//  TrackCell.swift
//  iTunesPreviewTutorial
//
//  Created by Lam Xuan on 7/4/16.
//  Copyright © 2016 JQ Software LLC. All rights reserved.
//

import UIKit

class CustomerCell: UITableViewCell {
        
    @IBOutlet weak var customerPhone: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var customerDateCreated: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
