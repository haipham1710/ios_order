//
//  Url.swift
//  Order
//
//  Created by Lam Xuan on 10/4/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
struct Url
{
    internal static var url = "http://apps.skyit.vn/";
    static var token = "Token";
    static var customer = "odata/PotentialCustomer"
    static var customerById = "odata/PotentialCustomer/ODataService.GetNameByPhone"
    static var customersByLastUpdated = "odata/PotentialCustomer/ODataService.GetLastUpdated"
}
