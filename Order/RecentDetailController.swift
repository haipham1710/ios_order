//
//  RecentDetailController.swift
//  Order
//
//  Created by admin on 11/3/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit
import MessageUI.MFMessageComposeViewController

class RecentDetailController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate {
    var phone: String?
    var name: String?
    var content: String?
    var phoneList: Array<PhoneModel> = []
    @IBOutlet weak var tableRecent: UITableView!
    @IBOutlet weak var lb_name: UILabel!
    @IBOutlet weak var lb_phone: UILabel!
    @IBOutlet weak var lb_content: UILabel!
    @IBAction func bt_mess(_ sender: AnyObject) {
        print("mess")
        launchMessageComposeViewController()
    }
    
    @IBAction func bt_call(_ sender: AnyObject) {
        if let url = URL(string: "tel://\(phone!)") {
            UIApplication.shared.openURL(url)
        }
        print("call")

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if(phone != nil)
        {
            lb_phone.text = phone
            lb_name.text = name
            lb_content.text = content
            phoneList.append(contentsOf: SQLiteHelper.getPhoneByAddress(address: phone!)!)

        }
        if(name?.isEmpty)!
        {
            let add: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addTapped))
            self.navigationItem.setRightBarButtonItems([add], animated: true)
        }

    }
    func addTapped(sender: UIButton) {
        performSegue(withIdentifier: "newPotentialCustomer", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "newPotentialCustomer") {
            let newCustomerController = segue.destination as! NewCustomerController;
            newCustomerController.phonePass = phone!
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.phoneList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = self.tableRecent.dequeueReusableCell(withIdentifier: "Cell") as! RecentCellController
        if phoneList.count != 0 && phoneList.count >= indexPath.row
        {
            let phone = phoneList[indexPath.row]
            // Update the textLabel text to use the title from the Album model
            cell.lb_date.text = phone.date
            cell.lb_duration.text = String(describing: phone.duration!) + " Giây"
            //cell.bt_add.addTarget(self, action: Selector(("newAddress")), for: .touchUpInside)
            // Start by setting the cell's image to a static file
            // Without this, we will end up without an image view!
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var chon:Int!
        chon = (indexPath as NSIndexPath).row
        tableRecent.deselectRow(at: indexPath, animated: true)

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func launchMessageComposeViewController() {
        if MFMessageComposeViewController.canSendText() {
            let messageVC = MFMessageComposeViewController()
            messageVC.messageComposeDelegate = self
            messageVC.recipients = [phone!]
            messageVC.body = ""
            self.present(messageVC, animated: true, completion: nil)
        }
        else {
            print("User hasn't setup Messages.app")
        }
    }
    
    // this function will be called after the user presses the cancel button or sends the text
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
