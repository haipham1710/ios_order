//
//  ServiceHelper.swift
//  GoVap
//
//  Created by ADMIN on 1/21/15.
//  Copyright (c) 2015 TMT Solution. All rights reserved.
//
import UIKit
class ServiceBuilder {
    var params : ParameterHelper = ParameterHelper();
    var request : NSMutableURLRequest;    init(url: String) {
        request = NSMutableURLRequest();
        request.URL = NSURL(string: url);
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        
    }
    
    func add(name: String,_ value: String?) -> ServiceBuilder {
        params.add(name, value);
        return self;
    }
    
    func add(name:String,_ value: Int) -> ServiceBuilder {
        params.add(name, value);
        return self;
    }
    
    func add(name:String,_ value: Bool?) -> ServiceBuilder {
        params.add(name, value);
        return self;
    }
    
    func setParameter(values: Parameter) -> ServiceBuilder {
        values.toParameter(params);
        return self;
    }
    
    func waitResponse<T: DataContract>(completed:T? -> Void) {
        makeRequest { (data:NSData?, error:NSError?) -> Void in
            if (error != nil) {
                completed(nil);
            } else {
                var helper: JsonHelper
                let result: T
                do
                {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary;
                    helper = JsonHelper(json: json);
                    result = T(json :helper);

                }
                catch let _error as NSError
                {
                    print(_error.localizedDescription)

                }
                completed(result);
            }
        }
    }
    
    func waitResponse<T : DataContract>(completed:([T] -> Void)) {
        makeRequest { (data:NSData?, error:NSError?) -> Void in
            if (error != nil) {
                completed([]);
            } else {
                var result = [T]();
                do
                {
                    let array = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSArray;
                    var helper : JsonHelper;
                    var t : T;
                    if array?.count > 0 {
                        for item in array! {
                            helper = JsonHelper(json: item as! NSDictionary);
                            t = T(json :helper);
                            result.append(t);
                        }
                    }

                }
                catch let _error as NSError
                {
                    print(_error.localizedDescription)
                }
                completed(result);
            }
        }
    }
    
    func waitResponse(completed: ErrorAction) {
        makeRequest { (data:NSData?, error:NSError?) -> Void in
            if (error != nil) {
                completed(ErrorResult.makeError(error!.description));
            } else {
                var result : ErrorResult;
                do{
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
                    
                    if (json == nil) {
                        // TODO: I'm tired
                        result = ErrorResult();
                    } else {
                        let helper = JsonHelper(json: json!);
                        result = ErrorResult(json: helper);
                    
                    }
                }
                catch let _error as NSError{
                    print(_error.localizedDescription)
                }
                
                
                completed(result);
            }
        }
    }
    
    private func makeRequest(completed: (NSData?, NSError?) -> Void) {
        if (params.count > 0) {
            let data = params.rawData;
            request.HTTPBody = data;
            request.setValue(String(data!.length), forHTTPHeaderField: "Content-Length");
            
        }
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
        { (response, data, error) in
            if (error != nil) {
                completed(nil, error);
            } else {
                completed(data, error);
            }
        }
    }
}

