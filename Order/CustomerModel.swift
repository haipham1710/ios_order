//
//  Customer.swift
//  Order
//
//  Created by Lam Xuan on 10/4/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
class CustomerModel : Parameter{
    var id: Int? = 0
    var name: String? = ""
    var numberPhone: String? = ""
    var content: String? = ""
    var potentialCustomerType: String? = ""
    var dateCreated: Date?
    var lastUpdated: Date?

    
    init(){}
    
    init(name: String, phone: String, potentialCustomerType: String,content: String, dateCreated: Date?, lastUpdated: Date? ) {
        self.name = name
        self.numberPhone = phone
        self.potentialCustomerType = potentialCustomerType
        self.content = content
        self.dateCreated = dateCreated
        self.lastUpdated = lastUpdated
        
    }
    init(json helper: JsonHelper) {
        id = helper.readInt("Id")
        name = helper.readString("Name")
        numberPhone = helper.readString("NumberPhone")
        content = helper.readString("Content")
        potentialCustomerType = helper.readString("PotentialCustomerType")
        dateCreated = helper.readDate("DateCreated")
        lastUpdated = helper.readDate("LastUpdated")
    }
    func toParameter(_ helper: ParameterHelper) {
        helper.add("Name", name)
        .add("Content", content)
        .add("NumberPhone",numberPhone)
        .add("PotentialCustomerType", potentialCustomerType)
        
    }
}
