//
//  RecentController.swift
//  Order
//
//  Created by Super on 10/31/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit

class RecentController: UIViewController,UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    @IBOutlet weak var tablePhone: UITableView!
    var phoneList: Array<PhoneModel> = []
    var page = 0
    var refreshControl: UIRefreshControl!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //Fetch customer
        phoneList.append(contentsOf: SQLiteHelper.getPhone(page)!)
        
        fetchPotentialCustomer()

        
        // get data from db and store into array used by UITableView
        

        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(RecentController.reload(_:)), for: UIControlEvents.valueChanged)
        tablePhone.addSubview(refreshControl)
        
    }
    func reload(_ sender:AnyObject) {
        page = 0
        phoneList.removeAll()
        phoneList.append(contentsOf: SQLiteHelper.getPhone(page)!)
        fetchPotentialCustomer()

        self.refreshControl.endRefreshing()

    }

    func fetchPotentialCustomer()
    {
        DispatchQueue.main.async(execute: {
            //Nếu PhoneList có thêm sdt mới thì chạy vòng for
            if(self.page*Fetcher.limit < self.phoneList.count)
            {
                for i in self.page*Fetcher.limit...self.phoneList.count-1 {
                    if(i == self.phoneList.count-1)
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    let customer: CustomerModel? = SQLiteHelper.getCustomerByPhone(phone: self.phoneList[i].phone)
                    if(customer != nil){
                        self.phoneList[i].name = (customer?.name)!
                        self.phoneList[i].content = (customer?.content)!
                        self.tablePhone.reloadData()
                    
                    }
                    
                }
            }

        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = phoneList.count - 1
        if (indexPath as NSIndexPath).row == lastElement {
            // handle your logic here
            print("last element")
            // Nếu PhoneList đã tới giới hạn thì tăng thêm 1 page
            if(phoneList.count == (page+1)*10)
            {
                page += 1
                phoneList.append(contentsOf: SQLiteHelper.getPhone(page)!)
                fetchPotentialCustomer()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.phoneList.count 
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = self.tablePhone.dequeueReusableCell(withIdentifier: "Cell") as! PhoneCellController
        if phoneList.count != 0 && phoneList.count >= indexPath.row
        {
            let phone = phoneList[indexPath.row]
            // Update the textLabel text to use the title from the Album model
            if(phone.name.isEmpty)
            {
                cell.lb_phone?.text =  phone.phone
                if(cell.bt_add != nil){
                    cell.bt_add.isHidden = false
                }

            }
            else
            {
                cell.lb_phone?.text =  phone.name
                if(cell.bt_add != nil){
                    cell.bt_add.isHidden = true
                }
                
            }
            if(phone.time != 1)
            {
                cell.lb_phone?.text? = (cell.lb_phone?.text)! + " (" + String(describing: phone.time!) + ")"
            }
            cell.lb_time?.text = phone.date
            if(cell.bt_add != nil)
            {
                cell.bt_add.tag = indexPath.row
            }
            //cell.bt_add.addTarget(self, action: Selector(("newAddress")), for: .touchUpInside)
            // Start by setting the cell's image to a static file
            // Without this, we will end up without an image view!
        
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "NewCustomer") {
            let bt_add: UIButton = sender as! UIButton

            let newCustomerController = segue.destination as! NewCustomerController;
            newCustomerController.phonePass = phoneList[bt_add.tag].phone
        }
        if(segue.identifier == "RecentDetail"){
            let indexPath: IndexPath = self.tablePhone.indexPath(for: sender as! UITableViewCell)!
            let recentDetailController = segue.destination as! RecentDetailController
            recentDetailController.phone = phoneList[indexPath.row].phone
            recentDetailController.name = phoneList[indexPath.row].name
            recentDetailController.content = phoneList[indexPath.row].content


        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.cellForRowAtIndexPath(indexPath)?.selectionStyle = .None
        //(sender as! UITableViewCell).selectionStyle = .None
        
        var chon:Int!
        chon = (indexPath as NSIndexPath).row
        tablePhone.deselectRow(at: indexPath, animated: true)
//        MessageBox.ask("Gọi đến số điện thoại: \(phoneList[chon].phone)", "Xác nhận") {
//            if let url = URL(string: "tel://\(self.phoneList[chon].phone)") {
//                UIApplication.shared.openURL(url)
//            }
//        }
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            //add code here for when you hit delete
            print("Delete")
        }
        // you need to implement this method too or you can't swipe to display the actions
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //customers.removeAll()
        
    }

}
