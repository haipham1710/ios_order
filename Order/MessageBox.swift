//
//  MessageBox.swift
//  Order
//
//  Created by Super on 10/21/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit

open class MessageBox: NSObject {
    fileprivate class StrongAlertView : UIAlertView {
        fileprivate var keepDelegate: MessageBoxDelegate?;
    }
    
    fileprivate class MessageBoxDelegate : NSObject, UIAlertViewDelegate {
        typealias Action = (() -> Void);
        fileprivate var okAction : Action?;
        fileprivate var yesAction : Action?;
        fileprivate var cancelAction: Action?;
        
        @objc fileprivate func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
            let title = alertView.buttonTitle(at: buttonIndex);
            if (title == "Yes") {
                yesAction?();
            } else if (title == "OK") {
                okAction?();
            } else if (title == "Cancel") {
                cancelAction?();
            }
        }
    }
    
    open class func show(_ message:String,_ title:String) {
        let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK");
        alert.show();
    }
    
    open class func ask(_ message:String?, _ title:String?,_ yesAction: @escaping (() -> Void)) {
        let delegate = MessageBoxDelegate();
        delegate.yesAction = yesAction;
        let alert = StrongAlertView(title: title, message: message, delegate: delegate, cancelButtonTitle: "Yes");
        alert.keepDelegate = delegate;
        alert.addButton(withTitle: "No");
        alert.show();
        
    }
}
