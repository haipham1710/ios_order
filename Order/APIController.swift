//
//  APIController.swift
//  iTunesPreviewTutorial
//
//  Created by Lam Xuan on 6/28/16.
//  Copyright © 2016 JQ Software LLC. All rights reserved.
//

import Foundation

class APIController
{
    enum enumContentType {
        case JSON
        case URLENCODED
    }
    private var method: String = "GET"
    private var params: ParameterHelper = ParameterHelper()
    private var credential: Credentials = Credentials(header: "",value: "")
    private var contentType: enumContentType = enumContentType.URLENCODED
    private var controller: String = ""
    init(url: String) {
        setController(controller: url)
    }
    func setController(controller: String)->APIController
    {
        self.controller = controller
        return self
    }
    func setContentType(contentType: enumContentType)->APIController
    {
        self.contentType = contentType
        return self
    }
    func add(name: String,_ value: String?)->APIController {
        params.add(name, value);
        return self
    }
    
    func add(name:String,_ value: Int)->APIController {
        params.add(name, value);
        return self
    }
    
    func add(name:String,_ value: Bool?)->APIController {
        params.add(name, value);
        return self
    }
    
    func setParameter(values: Parameter)->APIController {
        values.toParameter(params);
        return self
    }
    func setParameter(params: ParameterHelper)->APIController{
        self.params = params
        return self
    }
    func setMethod(method: String)->APIController
    {
        self.method = method
        return self
    }
    func setCredentials(credential: Credentials)->APIController
    {
        self.credential = credential
        return self
    }
    func makeRequest(complete: @escaping (Data?,NSError?) ->Void)
    {
        var url = NSURL(string: Url.url+controller)
        if(method == "GET")
        {
            if(params.count>0)
            {
                url = NSURL(string: (url?.absoluteString)! + "?" + setBodyContent(contentMap: params))
            }
        }
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url! as URL)
        request.timeoutInterval = 20.0
        // Set the method to POST
        request.httpMethod = self.method
        if(credential.hasObject()){
            credential.addCredentials(request)
        }
        // Set the POST body for the request
        
        if(method == "POST")
        {
            if (params.count > 0) {
                if(contentType == enumContentType.JSON)
                {
                    let data = params.rawData;
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.httpBody = data
                    request.setValue(String(data!.count), forHTTPHeaderField: "Content-Length");

                }
                else
                {
                    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    request.httpBody = setBodyContent(contentMap: params).data(using: String.Encoding.utf8);
                    request.setValue(String(setBodyContent(contentMap: params).data(using: String.Encoding.utf8)!.count), forHTTPHeaderField: "Content-Length")
                }
                //print(data)
                
            }
        }
        print(url?.absoluteString)
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main, completionHandler:
        { (response, data, error) in
            if let httpResponse = response as? HTTPURLResponse {
                print("responseCode \(httpResponse.statusCode)")
                if(200 <= httpResponse.statusCode && httpResponse.statusCode < 300)
                {
                    complete(data!,nil)
                    return
                }
                else {
                        // You can handle error response here
                        print("error \(error)")
                        complete(nil,error as NSError?)
                        
                    }

            }
        

        })
    }

    private func setBodyContent(contentMap: ParameterHelper) ->String{
        var contentBodyAsString: String = ""

        var firstOneAdded = false
        let contentKeys = contentMap.json.allKeys
        for contentKey in contentKeys {
            if(!firstOneAdded) {
                contentBodyAsString += (contentKey as! String) + "=" + (contentMap.json.value(forKey: contentKey as! String)! as! String)
                firstOneAdded = true
            }
            else {
                contentBodyAsString += "&" + (contentKey as! String) + "=" + (contentMap.json.value(forKey: contentKey as! String)! as! String)
            }
        }
        contentBodyAsString = contentBodyAsString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return contentBodyAsString
        //self.HTTPBody = contentBodyAsString.dataUsingEncoding(NSUTF8StringEncoding)
    }

}
