//
//  Fetcher.swift
//  Order
//
//  Created by Lam Xuan on 10/20/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
class Fetcher {
    static var limit = 30
    class func with(_ url: String)->APIController{
        return APIController(url: url)
    }
    
    class func token(params: ParameterHelper, tokenModel: @escaping (TokenModel?, NSError?) -> Void)
    {
        with(Url.token)
            .setParameter(params: params)
            .setMethod(method: "POST")
            .makeRequest{ (complete: (Data?, NSError?)) -> Void in
                if(complete.0 != nil)
                {
                    if let jsonResult = try! JSONSerialization.jsonObject(with: complete.0!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                            let jsonHelper : JsonHelper = JsonHelper(json: jsonResult)
                            let token : TokenModel = TokenModel(json: jsonHelper)
                            tokenModel(token, nil)
                        }
                }
                else
                {
                    tokenModel(nil, complete.1)
                }
        }
        
    }
    class func customer(_ customers: @escaping ([CustomerModel]?,NSError?) -> Void)
    {
        let token: String = UserPreference.token!
        let credential: Credentials = Credentials(header: "Authorization",value: "Bearer "+token)
        let params: ParameterHelper = ParameterHelper()
        params.add("$orderby", "DateCreated desc")
        with(Url.customer)
            .setMethod(method: "GET")
            .setParameter(params: params)
            .setCredentials(credential: credential)
            .makeRequest{ (complete: (Data?, NSError?)) -> Void in
                if(complete.0 != nil)
                {
                    if let jsonArray = try! (JSONSerialization.jsonObject(with: complete.0! as Data, options:JSONSerialization.ReadingOptions.mutableContainers) as AnyObject)["value"] as? NSArray {
                        var customerList = [CustomerModel]()
                        for jsonObject in jsonArray
                        {
                            let jsonHelper : JsonHelper = JsonHelper(json: jsonObject as! NSDictionary)
                            let customer : CustomerModel = CustomerModel(json: jsonHelper)
                            customerList.append(customer)
                        }
                        customers(customerList,nil)
                    }
                }
                else
                {
                    customers(nil, complete.1)
                }
            
        }
        
    }
    class func customerByPhone(number: String, customers: @escaping (CustomerModel?,NSError?) -> Void)
    {
        let params: ParameterHelper = ParameterHelper()
        params.add("number", number)
        let token: String = UserPreference.token!
        let credential: Credentials = Credentials(header: "Authorization",value: "Bearer "+token)
        with(Url.customerById)
            .setMethod(method: "GET")
            .setParameter(params: params)
            .setCredentials(credential: credential)
            .makeRequest{ (complete: (Data?, NSError?)) -> Void in
                if(complete.0 != nil)
                {
                    if let jsonArray = try! (JSONSerialization.jsonObject(with: complete.0!, options:JSONSerialization.ReadingOptions.mutableContainers) as AnyObject)["value"] as? NSArray {
                        var customerList = [CustomerModel]()
                        for jsonObject in jsonArray
                        {
                            let jsonHelper : JsonHelper = JsonHelper(json: jsonObject as! NSDictionary)
                            let customer : CustomerModel = CustomerModel(json: jsonHelper)
                            print(customer.name)
                            customerList.append(customer)
                        }
                        customers(customerList.count != 0 ? customerList[0] : nil,nil)
                    }
                }
                else
                {
                    customers(nil, complete.1)
                }
                
        }
        
    }
    class func customerByLastUpdate(date: Date, results: @escaping ([CustomerModel]?,NSError?) -> Void)
    {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let params: ParameterHelper = ParameterHelper()
        params.add("date", timeFormatter.string(from: date))
        let token: String = UserPreference.token!
        let credential: Credentials = Credentials(header: "Authorization",value: "Bearer "+token)
        with(Url.customersByLastUpdated)
            .setMethod(method: "GET")
            .setParameter(params: params)
            .setCredentials(credential: credential)
            .makeRequest{ (complete: (Data?, NSError?)) -> Void in
                if(complete.0 != nil)
                {
                    if let jsonArray = try! (JSONSerialization.jsonObject(with: complete.0!, options:JSONSerialization.ReadingOptions.mutableContainers) as AnyObject)["value"] as? NSArray {
                        var customerList = [CustomerModel]()
                        for jsonObject in jsonArray
                        {
                            let jsonHelper : JsonHelper = JsonHelper(json: jsonObject as! NSDictionary)
                            let customer : CustomerModel = CustomerModel(json: jsonHelper)
                            customerList.append(customer)
                        }
                        results(customerList.count != 0 ? customerList : nil,nil)
                    }
                }
                else
                {
                    results(nil, complete.1)
                }
                
        }
        
    }

    class func insertCustomer(customer: CustomerModel, results: @escaping (String?,NSError?) -> Void)
    {
        let token: String = UserPreference.token!
        let credential: Credentials = Credentials(header: "Authorization",value: "Bearer "+token)
        with(Url.customer)
            .setParameter(values: customer)
            .setCredentials(credential: credential)
            .setMethod(method: "POST")
            .setContentType(contentType: APIController.enumContentType.JSON)
            .makeRequest{ (complete: (Data?, NSError?)) -> Void in
                if(complete.0 != nil)
                {
                    if let jsonResult = NSString(data: complete.0!, encoding: String.Encoding.utf8.rawValue){
                        results(jsonResult as String, nil)
                    }
                }
                else
                {
                    results(nil, complete.1)

                }
        }
        
    }
}

    
