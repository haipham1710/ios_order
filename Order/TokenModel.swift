//
//  TokenModel.swift
//  Order
//
//  Created by Lam Xuan on 10/20/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
class TokenModel {
    var access_token: String = ""
    var userName: String = ""
    var error_description: String = ""
    init(json helper: JsonHelper) {
        access_token = helper.readString("access_token")!;
        userName = helper.readString("userName")!;
        error_description = helper.readString("error_description")!
    }
}
