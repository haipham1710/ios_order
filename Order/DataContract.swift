//
//  DataContract.swift
//  GoVap
//
//  Created by ADMIN on 1/21/15.
//  Copyright (c) 2015 TMT Solution. All rights reserved.
//

protocol DataContract {
    init(json helper:JsonHelper);
}
