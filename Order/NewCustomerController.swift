//
//  DialogController.swift
//  Order
//
//  Created by Lam Xuan on 10/19/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit
class NewCustomerController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var tf_type: UITextField!
    @IBOutlet weak var tf_content: UITextField!
    @IBOutlet weak var tf_phone: UITextField!
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var bt_add: UIButton!
    var toolBar = UIToolbar()
    
    var typePicker: UIPickerView!
    
    var types = ["Loại 1", "Loại 2"]
    

    var params : ParameterHelper = ParameterHelper();
    var phonePass: String = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewCustomerController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        bt_add.addTarget(self, action: #selector(actionAdd), for: .touchUpInside)
        tf_name.delegate = self
        tf_phone.delegate = self
        tf_content.delegate = self
        tf_type.delegate = self
        typePicker = UIPickerView()
        tf_type.inputView = typePicker
        typePicker.delegate = self
        typePicker.dataSource = self
        tf_type.text = types[0]
        if(!phonePass.isEmpty)
        {
            tf_phone.text = phonePass
        }
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func actionAdd(_ action:UIButton)
    {
        let name = tf_name.text
        let phone = tf_phone.text
        let content = tf_content.text
        let type = typePicker.selectedRow(inComponent: 0)+1
        if(name == nil || (name?.isEmpty)!)
        {
            tf_name.layer.borderWidth = 1
            tf_name.layer.borderColor = UIColor.red.cgColor
            return
        }
        let currentDate = Date()
        let customer: CustomerModel = CustomerModel(name: name!, phone: phone!, potentialCustomerType: String(type),content: content!,dateCreated: currentDate,lastUpdated: currentDate)
        if Utils.isConnectedToNetwork() == true
        {

            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            Fetcher.insertCustomer(customer: customer, results: {(results: String?, error: NSError?)->Void in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if(results != nil)
                {
                    MessageBox.show("Thêm khách hàng thành công", "Thông báo")
                    SQLiteHelper.syncCustomer(customer: customer, result: { (success, error) in
                        print("New customer: save to local")
                    })
                }
                else
                {
                    MessageBox.show("Thêm khách hàng thất bại", "Lỗi")
                }
            })
        }
        else
        {
            SQLiteHelper.insertCustomerOffline(customer: customer, result: { (success, error) in
                if(success)
                {
                    MessageBox.show("Lưu tạm thành công", "Thông báo")
                }
                else
                {
                    MessageBox.show("Lưu tạm thất bại. \nLỗi: \(error)", "Thông báo")

                }
            })
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == tf_name)
        {
            tf_name.layer.borderWidth=0
        }
        return true
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return types.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return types[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tf_type.text = types[row]
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == tf_name) {
            tf_phone.becomeFirstResponder()
        } else if (textField == tf_phone) {
            //remove keyboard
            tf_content.becomeFirstResponder()
        } else if (textField == tf_content) {
            //remove keyboard
            tf_type.becomeFirstResponder()
        } else
        {
            tf_type.resignFirstResponder()
 
        }
        return false
    }

}
