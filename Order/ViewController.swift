//
//  ViewController.swift
//  Order
//
//  Created by Lam Xuan on 6/27/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


@available(iOS 8.0, *)
class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    var refreshControl: UIRefreshControl!

    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var bottomLayout: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    var customers = [CustomerModel]()
    var customersOrig = [CustomerModel]()

    var toDatePickerView = UIDatePicker()
    var fromDatePickerView = UIDatePicker()

    var toolBar = UIToolbar()
    @IBOutlet var fromDateTextField: UITextField!
    @IBOutlet var toDateTextField: UITextField!
    var fromDate: Date!
    var toDate: Date!
    var alert = UIAlertController()
    let timeFormatter = DateFormatter()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)

        //Add search and add button in right navigation bar
        let rightSearchBarButtonItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(ViewController.searchTapped))
        let add: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addTapped))
        self.navigationItem.setRightBarButtonItems([add,rightSearchBarButtonItem], animated: true)

        //self.navigationItem.setRightBarButtonItems([add], animated: true)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //Fetch customer
        if(Utils.isConnectedToNetwork())
        {
            Fetcher.customer( { (customers: [CustomerModel]?,error: NSError?) in
                if(customers != nil)
                {
                    self.loadTableWithData(customers!)
                    self.customersOrig = customers!
                }
                else
                {
                    MessageBox.show((error?.localizedDescription)!, "Lỗi")
                }
            })
        }
        else
        {
            self.loadTableWithData(SQLiteHelper.getCustomerList())
            self.customersOrig = SQLiteHelper.getCustomerList()

        }
        //Add keyboard notification
        registerForKeyboardNotifications()
        
        //init toolbar and datepicker
        self.toDatePickerView.datePickerMode = UIDatePickerMode.date
        self.fromDatePickerView.datePickerMode = UIDatePickerMode.date
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.blue
        toolBar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.done, target: self, action: #selector(ViewController.cancelButtonToolbar(_:)))
        toolBar.setItems([spaceButton,spaceButton,cancelButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        timeFormatter.dateFormat = "dd/MM/yyyy"
        //add pull to refresh for tableview
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(ViewController.pullToReload(_:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //add toolbar for keyboard
        searchView.inputAccessoryView = self.toolBar


    }
    func pullToReload(_ sender:AnyObject) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if(Utils.isConnectedToNetwork())
        {
            SyncCustomer()
            Fetcher.customer( { (customers: [CustomerModel]?,error: NSError?) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if(customers != nil)
                {
                    self.loadTableWithData(customers!)
                    self.customersOrig = customers!
                    self.refreshControl.endRefreshing()

                }
                else
                {
                MessageBox.show((error?.localizedDescription)!, "Lỗi")
            }
            })
        }
        else
        {
            self.loadTableWithData(SQLiteHelper.getCustomerList())
            self.customersOrig = SQLiteHelper.getCustomerList()
            self.refreshControl.endRefreshing()
        }
    }
    //download new customer on server and upload new customer on local
    func SyncCustomer()
    {
        Fetcher.customerByLastUpdate(date: SQLiteHelper.getLastSync()) { (customerList: [CustomerModel]?, error: NSError?) in
            if(customerList != nil)
            {
                // insert data
                SQLiteHelper.syncCustomers(customers: customerList!)
                // end insert data
            }
        }
        //if has new customer in local, sync to server and delete in local
        let customerList : Array<CustomerModel> = SQLiteHelper.getCustomersOffline()
        if(customerList.count != 0)
        {
            for customer in customerList {
                Fetcher.insertCustomer(customer: customer, results: { (result, error) in
                    if(result != nil)
                    {
                        SQLiteHelper.deleteCustomerOffline(phone: customer.numberPhone!)
                    }
                    
                })
            }
        }
    
    }

    func resetFromDateValue(_ forDate: Date)
    {
        var dC = DateComponents()
        let cC = Calendar.current
        let year = Int((cC as NSCalendar).component(NSCalendar.Unit.year, from: forDate))
        let month = Int((cC as NSCalendar).component(NSCalendar.Unit.month, from: forDate))
        let day = Int((cC as NSCalendar).component(NSCalendar.Unit.day, from: forDate))
        dC.year = year
        dC.month = month
        dC.day = day
        dC.hour = 0
        dC.minute = 0
        dC.second = 0
        fromDate = cC.date(from: dC)
    }
    func resetToDateValue(_ forDate: Date)
    {
        var dC = DateComponents()
        let cC = Calendar.current
        let year = Int((cC as NSCalendar).component(NSCalendar.Unit.year, from: forDate))
        let month = Int((cC as NSCalendar).component(NSCalendar.Unit.month, from: forDate))
        let day = Int((cC as NSCalendar).component(NSCalendar.Unit.day, from: forDate))
        dC.year = year
        dC.month = month
        dC.day = day
        dC.hour = 23
        dC.minute = 59
        dC.second = 59
        toDate = cC.date(from: dC)
    }
    func cancelButtonToolbar(_ action:UIBarButtonItem)
    {
        fromDate = nil
        toDate = nil
        if(fromDateTextField != nil && toDateTextField != nil)
        {
            fromDateTextField.text = ""
            toDateTextField.text = ""
            fromDateTextField.endEditing(true)
            toDateTextField.endEditing(true)

        }
        loadTableWithData(customers)
        searchView.endEditing(true)
    }
    deinit {
        self.deregisterFromKeyboardNotifications()
    }
    func fromDate(_ textField: UITextField!){
        // add the text field and make the result global
        textField.placeholder = "Từ ngày:"
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.text = (fromDate != nil) ? timeFormatter.string(from: fromDate) : ""
        textField.inputView = self.fromDatePickerView
        textField.inputAccessoryView = self.toolBar
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
        self.fromDatePickerView.addTarget(self, action: #selector(ViewController.handleDatePicker(_:)), for: UIControlEvents.valueChanged)
        fromDateTextField = textField
        
    }
    
    func toDate(_ textField: UITextField!){
        // add the text field and make the result global
        textField.placeholder = "Đến ngày:"
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.text = (toDate != nil ) ? timeFormatter.string(from: toDate) : ""
        textField.inputView = self.toDatePickerView
        textField.inputAccessoryView = self.toolBar
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
        self.toDatePickerView.addTarget(self, action: #selector(ViewController.handleDatePicker2(_:)), for: UIControlEvents.valueChanged)
        toDateTextField = textField
    }
    func searchTapped(_ sender:UIButton) {
        alert = UIAlertController(title: "Tìm kiếm", message: "Chọn ngày", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {action in self.btn_ok()}))
        alert.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.default, handler: nil))

        alert.addTextField(configurationHandler: fromDate)
        alert.addTextField(configurationHandler: toDate)
        self.present(alert, animated: true, completion: nil)
        //remove the ugly border of textfield
        for textfield: UIView in alert.textFields! {
            let container: UIView = textfield.superview!
            let effectView: UIView = container.superview!.subviews[0]
            container.backgroundColor = UIColor.clear
            effectView.removeFromSuperview()
        }


    }
    func addTapped(_ sender:UIButton) {
        performSegue(withIdentifier: "nextView", sender: self)
    }
    func btn_ok()
    {
        if(fromDateTextField.text != "" && toDateTextField.text != "")
        {
            let filterCustomer = customersOrig.filter { customer in
                if(customer.dateCreated<=toDate && customer.dateCreated>=fromDate)
                {
                    return true
                }
                return false
            }
            self.loadTableWithData(filterCustomer)
        }
        else if(fromDateTextField.text != "" && toDateTextField.text == "")
        {
            toDate = nil
            let filterCustomer = customersOrig.filter { customer in
                if(customer.dateCreated>=fromDate)
                {
                    return true
                }
                return false
            }
            self.loadTableWithData(filterCustomer)

        }
        else if(fromDateTextField.text == "" && toDateTextField.text != "")
        {
            fromDate = nil
            let filterCustomer = customersOrig.filter { customer in
                if(customer.dateCreated<=toDate)
                {
                    return true
                }
                return false
            }
            self.loadTableWithData(filterCustomer)

        }
        else {
            fromDate = nil
            toDate = nil
            self.loadTableWithData(customersOrig)
        }
    }
    func handleDatePicker(_ sender: UIDatePicker) {
        fromDateTextField.text = timeFormatter.string(from: sender.date)
        resetFromDateValue(sender.date)

    }
    func handleDatePicker2(_ sender: UIDatePicker) {
        toDateTextField.text = timeFormatter.string(from: sender.date)
        resetToDateValue(sender.date)
    }
    func registerForKeyboardNotifications()
    {
    //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }


    func deregisterFromKeyboardNotifications()
    {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func keyboardWasShown(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        bottomLayout.constant = keyboardSize - bottomLayoutGuide.length
        
        let duration: TimeInterval = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        UIView.animate(withDuration: duration, animations: { self.view.layoutIfNeeded() }) 
        
    }


    func keyboardWillBeHidden(_ notification: Notification)
    {
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let duration: TimeInterval = (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        bottomLayout.constant = 0
        UIView.animate(withDuration: duration, animations: { self.view.layoutIfNeeded() }) 
        
    }

    func loadTableWithData(_ customers: [CustomerModel]) {

        DispatchQueue.main.async(execute: {
            self.customers = customers
            self.tableView!.reloadData()
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
            return self.customers.count
        }    
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell") as! CustomerCell
        var arrayOfCustomer = [CustomerModel]()
            arrayOfCustomer = self.customers
        
        if arrayOfCustomer.count != 0 && arrayOfCustomer.count >= (indexPath as NSIndexPath).row
        {
            let customer = arrayOfCustomer[(indexPath as NSIndexPath).row]
            // Update the textLabel text to use the title from the Album model
            cell.customerName?.text = customer.name
            cell.customerPhone?.text = customer.numberPhone
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            if(customer.dateCreated != nil)
            {
                cell.customerDateCreated?.text = "Ngày tạo: " + timeFormatter.string(from: customer.dateCreated!)
            }
            // Start by setting the cell's image to a static file
            // Without this, we will end up without an image view!
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "RecentDetail"){
            let indexPath: IndexPath = self.tableView.indexPath(for: sender as! UITableViewCell)!
            let recentDetailController = segue.destination as! RecentDetailController
            recentDetailController.phone = customers[indexPath.row].numberPhone
            recentDetailController.name = customers[indexPath.row].name
            recentDetailController.content = customers[indexPath.row].content
            
            
        }


    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.cellForRowAtIndexPath(indexPath)?.selectionStyle = .None
        //(sender as! UITableViewCell).selectionStyle = .None

//        var chon:Int!
//        chon = (indexPath as NSIndexPath).row
//        MessageBox.ask("Gọi đến số điện thoại: \(customers[chon].numberPhone!)", "Xác nhận") {
//            if let url = URL(string: "tel://\(self.customers[chon].numberPhone!)") {
//                UIApplication.shared.openURL(url)
//            }
//        }
        tableView.deselectRow(at: indexPath, animated: true)

        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //customers.removeAll()
        if !searchText.isEmpty
        {
            let filterCustomer = customersOrig.filter { customer in
                return (customer.name!.lowercased().contains(searchText.lowercased()) || (customer.numberPhone?.contains(searchText))!)
            }
            self.loadTableWithData(filterCustomer)
        }
        else
        {
            self.loadTableWithData(customersOrig)
        }
    }
}


