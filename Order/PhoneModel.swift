//
//  PhoneModel.swift
//  Order
//
//  Created by Super on 10/31/16.
//  Copyright © 2016 Hai. All rights reserved.
//

import Foundation
class PhoneModel: AnyObject{
    var phone: String = ""
    var name: String = ""
    var date: String = ""
    var time: Int32?
    var duration: Int32?
    var content: String?
    init(phone: String,date: String,time: Int32?, duration: Int32?, content: String?)
    {
        self.phone = phone
        self.date = date
        self.time = time
        self.duration = duration
        self.content = content
    }
}
