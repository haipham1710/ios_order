//
//  ErrorResult.swift
//  GoVap
//
//  Created by ADMIN on 1/21/15.
//  Copyright (c) 2015 TMT Solution. All rights reserved.
//



class ErrorResult: DataContract {
    
    var isError: Bool = false;
    var message: String = "";
    var entityId: Int = 0;
    
    init(){}
    
    required init(json helper: JsonHelper) {
        isError = helper.readBool("IsError");
        message = helper.readString("Message") ?? "";
        entityId = helper.readInt("EntityId");
    }
    
    class func makeError(message: String) -> ErrorResult {
        let result = ErrorResult();
        result.isError = true;
        result.message = message;
        return result;
    }
    
    class  func makeSuccess(message: String) -> ErrorResult {
        return makeSuccess(message, 0);
    }
    class  func makeSuccess(message: String,_ entityId:Int) -> ErrorResult {
        let result = ErrorResult();
        result.isError = false;
        result.message = message;
        result.entityId = entityId;
        return result;
    }
}
