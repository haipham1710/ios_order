//
//  JsonHelper.swift
//  GoVap
//
//  Created by ADMIN on 1/21/15.
//  Copyright (c) 2015 TMT Solution. All rights reserved.
//
import UIKit
var token_t : Int = 0;
class JsonHelper: NSObject {
    private static var __once: () = { () -> Void in
            JsonHelper.iso8601Formatter = DateFormatter();
            JsonHelper.iso8601Formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
            JsonHelper.alternativeIso8601Formatter = DateFormatter();
            JsonHelper.alternativeIso8601Formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
            
        }()
    static var iso8601Formatter : DateFormatter!;
    static var alternativeIso8601Formatter : DateFormatter!;
    fileprivate var json : NSDictionary;
    
    func initialize() {
        _ = JsonHelper.__once;
    }
    
    init(data:Data) {
        json = NSDictionary();
        do{
        json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary;
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
        
        super.init();
        initialize();
    }
    
    init(json:NSDictionary) {
        self.json = json;
        super.init();
        initialize();
    }
    
    override var description : String {
        get {
            return json.description;
        }
    }
    
    fileprivate func readNumber(_ name: String) -> NSNumber? {
        return json.object(forKey: name) as? NSNumber;
    }
    
    func readString(_ name:String) -> String? {
        return json.object(forKey: name) as? String ?? ""
    }
    
    func readBool(_ name:String) -> Bool {
        return readNumber(name)?.boolValue ?? false;
    }
    
    func readInt(_ name:String) -> Int {
        return readNumber(name)?.intValue ?? 0;
    }
    
    func readDouble(_ name:String) -> Double {
        return readNumber(name)?.doubleValue ?? 0;
    }
    
    func readDate(_ name:String) -> Date? {
        let value = readString(name);
        if (value?.isEmpty ?? true) {
            return nil;
        }

        let date = JsonHelper.iso8601Formatter.date(from: value!) ?? JsonHelper.alternativeIso8601Formatter.date(from: value!);
        return date;
    }
}
